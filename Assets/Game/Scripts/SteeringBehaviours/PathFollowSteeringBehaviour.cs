using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PathFollowSteeringBehaviour : ArriveSteeringBehaviour
{
	public List<Transform> points = new List<Transform>();
	public float WaypointDistance = 0.5f;
	public bool loop = false;
	public bool useNavMesh = false;

	private int currentWaypointIndex = 0;
	private int pathIndex = 0;
	private NavMeshPath path;

	public override void Init(SteeringAgent agent)
	{
		base.Init(agent);
		path = new NavMeshPath();
		target = steeringAgent.transform.position;
	}

	public override Vector3 calculateForce()
	{
        if (useNavMesh)
        {
			// Not go through all the points yet but reach the end of current path, re-calculate path
			if (currentWaypointIndex < points.Count && pathIndex == 0)
            {
				NavMesh.CalculatePath(steeringAgent.transform.position, points[currentWaypointIndex].position, NavMesh.AllAreas, path);
				if (path.corners.Length > 0)
				{
					target = path.corners[0];
				}
				else
				{
					target = steeringAgent.transform.position;
				}
			}
			
			// Reach the path point
			if (IsReachTarget(target))
			{
				// not yet finish path
				if (pathIndex != path.corners.Length)
				{
					pathIndex++;
					if (pathIndex < path.corners.Length)
					{
						target = path.corners[pathIndex];
					}
				}
				// finish path
				else
				{
					pathIndex = 0;
					currentWaypointIndex++;
				}
			}

			// Reach the last points bakc to 1st if loop
			if (currentWaypointIndex == points.Count && loop)
			{
				target = points[0].position;
				pathIndex = 0;
				currentWaypointIndex = 0;
			}
		}
        else
        {
			// Not go through all the points yet
			if (currentWaypointIndex <= points.Count && IsReachTarget(target))
			{
				if (currentWaypointIndex < points.Count)
				{
					target = points[currentWaypointIndex].position;
				}
				currentWaypointIndex++;
			}

			// Reach the last ones
			if (currentWaypointIndex > points.Count && loop)
			{
				target = points[0].position;
				currentWaypointIndex = 0;
			}
		}

		return CalculateArriveForce();
	}

	private bool IsReachTarget(Vector3 _target)
    {
		return (target - steeringAgent.transform.position).magnitude < WaypointDistance;
	}

	protected override void OnDrawGizmos()
	{
		base.OnDrawGizmos();

        if (useNavMesh)
        {
			if (path != null)
			{
				for (int i = 1; i < path.corners.Length; i++)
				{
					Debug.DrawLine(path.corners[i - 1], path.corners[i], Color.black);
				}
			}
		}
        else
        {
			if (points.Count > 0)
			{
				for (int i = 1; i < points.Count; i++)
				{
					Debug.DrawLine(points[i - 1].position, points[i].position, Color.black);
				}
			}
		}
	}
}
