using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderSteeringBehaviour : ArriveSteeringBehaviour
{
	public Transform LimitCenter;
	public float LimitRadius;
	public bool isSphere;
	public float WanderDistance = 2.0f;
	public float WanderRadius = 1.0f;
	public float WanderJitter = 20.0f;
	private Vector3 WanderTarget;
	private bool isCorrecting = false;

	private void Start()
	{
	}

	public override Vector3 calculateForce()
	{
        if (steeringAgent.reachedGoal)
        {
			isCorrecting = false;
		}

		// if Agent not in range and is not correcting path
        if (!AmIInRange(isSphere) && !isCorrecting)
        {
			target = GetRandomPointInRange(isSphere);
			isCorrecting = true;
		}
        // Agent is back to range
        else if (!isCorrecting)
        {
			steeringAgent.reachedGoal = false;
			float jitterThisTimeSlice = WanderJitter * Time.deltaTime;

			WanderTarget = WanderTarget + new Vector3(Random.Range(-1.0f, 1.0f) * jitterThisTimeSlice,
													  0.0f,
													  Random.Range(-1.0f, 1.0f) * jitterThisTimeSlice);
			WanderTarget.Normalize();
			WanderTarget *= WanderRadius;

			target = WanderTarget + new Vector3(0, 0, WanderDistance);
			target = steeringAgent.transform.rotation * target + steeringAgent.transform.position;

			Vector3 wanderForce = (target - steeringAgent.transform.position).normalized;
			return wanderForce *= steeringAgent.maxSpeed;
		}

		return CalculateArriveForce();
	}

	private Vector3 GetRandomPointInRange(bool _isSphere)
    {
		float theta = (float)Random.value * Mathf.PI * 2;
		float length = Random.Range(0.0f, LimitRadius);
		if (_isSphere)
		{
			return new Vector3(length * Mathf.Cos(theta), 0.0f, length * Mathf.Sin(theta)) + LimitCenter.position;
		}
		else
		{
			return new Vector3(length - LimitRadius * 0.5f, 0.0f, length - LimitRadius * 0.5f) + LimitCenter.position;
		}
	}

	private bool AmIInRange(bool _isSphere)
    {
		return _isSphere ? Vector3.Distance(steeringAgent.transform.position, LimitCenter.position) < LimitRadius : new Bounds(LimitCenter.position, new Vector3(LimitRadius, 2 * steeringAgent.transform.position.y, LimitRadius)).Contains(steeringAgent.transform.position);
	}

	protected override void OnDrawGizmos()
	{
        if (isSphere)
        {
			DebugExtension.DrawCircle(LimitCenter.position, Vector3.up, Color.green, LimitRadius);
        }
        else
        {
			DebugExtension.DrawBounds(new Bounds(LimitCenter.position, new Vector3(LimitRadius, 0, LimitRadius)), Color.green);
        }

		Vector3 circleCenter = transform.rotation * new Vector3(0, 0, WanderDistance) + transform.position;
		DebugExtension.DrawCircle(circleCenter, Vector3.up, Color.red, WanderRadius);
		Debug.DrawLine(transform.position, circleCenter, Color.yellow);
		Debug.DrawLine(transform.position, target, Color.blue);
	}
}
